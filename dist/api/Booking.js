import { DateTimeResolver } from "graphql-scalars";
import { extendType, floatArg, list, nonNull, objectType, stringArg, } from "nexus";
export const Booking = objectType({
    name: "Booking",
    definition(t) {
        t.string("user_email");
        t.string("room_uuid");
        t.nonNull.field("created_at", { type: "DateTime" });
        t.nonNull.field("start_date", { type: "DateTime" });
        t.nonNull.field("end_date", { type: "DateTime" });
        t.field("rooms", {
            type: nonNull(list("Room")),
            resolve(_root, _args, ctx) {
                return ctx.db.rooms.findMany({ where: { uuid: _root.room_uuid } });
            },
        });
    },
});
export const BookingQuery = extendType({
    type: "Query",
    definition(t) {
        t.field("bookings", {
            type: nonNull(list("Booking")),
            resolve(_root, _args, ctx) {
                return ctx.db.bookings.findMany();
            },
        });
    },
});
export const PostMutation = extendType({
    type: "Mutation",
    definition(t) {
        t.field("createBooking", {
            type: "Booking",
            args: {
                start_date: nonNull(floatArg()),
                end_date: nonNull(floatArg()),
                user_email: nonNull(stringArg()),
            },
            async resolve(_root, args, ctx) {
                const userStartDateObject = DateTimeResolver.parseValue(new Date(args.start_date).toISOString());
                const userEndDateObject = DateTimeResolver.parseValue(new Date(args.end_date).toISOString());
                const bookingsInPeriod = ctx.db.bookings.findMany({
                    where: {
                        AND: [
                            {
                                OR: [
                                    {
                                        start_date: {
                                            gte: userStartDateObject,
                                            lte: userEndDateObject,
                                        },
                                    },
                                    {
                                        end_date: {
                                            gte: userStartDateObject,
                                            lte: userEndDateObject,
                                        },
                                    },
                                ],
                            },
                            {
                                start_date: {
                                    lte: userStartDateObject,
                                },
                                end_date: {
                                    gte: userEndDateObject,
                                },
                            },
                        ],
                    },
                });
                const bookedRooms = await bookingsInPeriod.then((bookings) => bookings.map((booking) => booking.room_uuid));
                const freeRooms = ctx.db.rooms.findMany({
                    where: {
                        uuid: {
                            not: {
                                in: bookedRooms,
                            },
                        },
                    },
                });
                const freeRoomUuid = freeRooms.then((room) => {
                    return room[0].uuid;
                });
                if (freeRooms.length === 0) {
                    throw new Error("No room available");
                }
                return ctx.db.bookings.create({
                    data: {
                        user_email: args.user_email,
                        start_date: userStartDateObject,
                        end_date: userEndDateObject,
                        rooms: { connect: { uuid: await freeRoomUuid } },
                    },
                });
            },
        });
    },
});
