import { bookings } from "@prisma/client";
import { DateTimeResolver } from "graphql-scalars";
import { DateTime } from "./schema";
import {
  extendType,
  floatArg,
  list,
  nonNull,
  objectType,
  stringArg,
} from "nexus";

// Definition of the Booking type
export const Booking = objectType({
  name: "Booking", // <- Name of your type
  definition(t) {
    t.string("user_email");
    t.string("room_uuid");
    t.nonNull.field("created_at", { type: "DateTime" });
    t.nonNull.field("start_date", { type: "DateTime" });
    t.nonNull.field("end_date", { type: "DateTime" });
    t.field("rooms", {
      type: nonNull(list("Room")),
      resolve(_root, _args, ctx) {
        // 1
        return ctx.db.rooms.findMany({ where: { uuid: _root.room_uuid } });
      },
    });
  },
});

// Query for finding all bookings
export const BookingQuery = extendType({
  type: "Query",
  definition(t) {
    t.field("bookings", {
      type: nonNull(list("Booking")),
      resolve(_root, _args, ctx) {
        // 1
        return ctx.db.bookings.findMany();
      },
    });
  },
});

// This is where we define the mutation for creating a booking
export const PostMutation = extendType({
  type: "Mutation",
  definition(t) {
    t.field("createBooking", {
      type: "Booking",
      args: {
        // Date in milliseconds from 1970
        start_date: nonNull(floatArg()),
        end_date: nonNull(floatArg()),
        user_email: nonNull(stringArg()),
      },
      async resolve(_root, args, ctx) {
        // TODO : Add type
        const userStartDateObject = DateTimeResolver.parseValue(
          new Date(args.start_date).toISOString()
        );
        const userEndDateObject = DateTimeResolver.parseValue(
          new Date(args.end_date).toISOString()
        );

        const bookingsInPeriod: Promise<bookings[]> = ctx.db.bookings.findMany({
          where: {
            AND: [
              // This AND condition checks if either of the two conditions inside the array is true
              {
                OR: [
                  // This OR condition checks if the booking's startDate falls inside the user-provided date range
                  {
                    start_date: {
                      gte: userStartDateObject, // startDate should be greater than or equal to userStartDate
                      lte: userEndDateObject, // startDate should be less than or equal to userEndDate
                    },
                  },
                  // This OR condition checks if the booking's endDate falls inside the user-provided date range
                  {
                    end_date: {
                      gte: userStartDateObject, // endDate should be greater than or equal to userStartDate
                      lte: userEndDateObject, // endDate should be less than or equal to userEndDate
                    },
                  },
                ],
              },
              // This condition checks if the booking spans the entire user-provided date range
              {
                start_date: {
                  lte: userStartDateObject, // startDate should be less than or equal to userStartDate
                },
                end_date: {
                  gte: userEndDateObject, // endDate should be greater than or equal to userEndDate
                },
              },
            ],
          },
        });

        // Creates a list of room UUIDs that are booked in the user-provided date range
        const bookedRooms: string[] = await bookingsInPeriod.then((bookings) =>
          bookings.map((booking) => booking.room_uuid)
        );

        // Find all rooms that are not booked in the user-provided date range
        const freeRooms = ctx.db.rooms.findMany({
          where: {
            uuid: {
              not: {
                in: bookedRooms,
              },
            },
          },
        });
        // Get the UUID of the first room in the list of free rooms
        const freeRoomUuid = freeRooms.then((room) => {
          return room[0].uuid;
        });

        // If no room is available, throw an error
        if (freeRooms.length === 0) {
          throw new Error("No room available");
        }

        // Create a booking with the user-provided details and the UUID of the first free room
        return ctx.db.bookings.create({
          data: {
            user_email: args.user_email,
            start_date: userStartDateObject,
            end_date: userEndDateObject,
            rooms: { connect: { uuid: await freeRoomUuid } },
          },
        });
      },
    });
  },
});
