import { server } from "./api/server.js";
import { startStandaloneServer } from "@apollo/server/standalone";
import { context } from "./api/context.js";

const port = Number.parseInt(process.env.PORT) || 4000;

// Passing an ApolloServer instance to the `startStandaloneServer` function:
//  1. creates an Express app
//  2. installs your ApolloServer instance as middleware
//  3. prepares your app to handle incoming requests
const { url } = await startStandaloneServer(server, {
  listen: { port },
  context: async () => {
    return context;
  },
});

console.log(`🚀  Server ready at: ${url}`);
